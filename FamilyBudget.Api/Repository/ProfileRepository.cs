using System.Threading.Tasks;
using System.Collections.Generic;
using FamilyBudget.Entities;
using FamilyBudget.Api.Repository.Interfaces;
using MySqlConnector;
using Dapper;
using Dapper.Contrib.Extensions;
using System.Linq;
using System.Data.Common;
using FamilyBudget.Api.DataAccess.Interfaces;


namespace FamilyBudget.Api.Repository
{
    public class ProfileRepository : IProfileRepository
    {
        private readonly IData _data;
        public ProfileRepository(IData data){
            _data = data;
        }

        public DbConnection DbConnection => _data.DbConnection;

        public async Task<Profile> Add(Profile profile)
        {
            profile.Deleted = 0;
            await _data.DbConnection.InsertAsync<Profile>(profile);

            return profile;
        }

        public async Task<Profile> Delete(int id)
        {

            var profile = await GetById(id);

            if(profile == null){
                return profile;
            }else{
                profile.Deleted = 1;
            }

            await _data.DbConnection.UpdateAsync<Profile>(profile);

            // var result = await _conn.DeleteAsync<Profile>( new Profile{ Id = id});

            return profile;
        }

        public async Task<IEnumerable<Profile>> GetAll()
        {

            //var profiles = await _conn.GetAllAsync<Profile>();

            // profiles = profiles.Where(p => p.Delete == false);

            var template = new Profile { Deleted = 0};
            var parameters = new DynamicParameters(template);
            var sql = "SELECT * FROM profile where Deleted = @Deleted";

            var profiles = await _data.DbConnection.QueryAsync<Profile>(sql, parameters);

            return profiles;
        } 

        public async Task<Profile> GetById(int id)
        {
            var template = new Profile { Deleted = 0, Id = id};
            var parameters = new DynamicParameters(template);
            var sql = "SELECT * FROM profile where Id = @Id AND Deleted = @Deleted";

            var profiles = await _data.DbConnection.QueryAsync<Profile>(sql, parameters);

            var profile = profiles.FirstOrDefault();

            //var profile = await _conn.GetAsync<Profile>(id);

            return profile;
        }

        public async Task<Profile> Update(Profile profile)
        {
            await _data.DbConnection.UpdateAsync<Profile>(profile);

            return profile;
        }
    }
}